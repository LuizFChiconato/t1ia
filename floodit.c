#include <stdio.h>
#include <stdlib.h>

typedef struct TGame {
  int numColors;
  int numLines;
  int numColumns;
  int **table;
} TGame;

void readGame (TGame *game) {
    scanf("%d", &(game->numLines));
    scanf("%d", &(game->numColumns));
    scanf("%d", &(game->numColors));

    int i = 0;
    int j = 0;

    game->table = malloc (game->numLines * sizeof (int*)) ;
    for (i = 0; i < game->numLines; i++) {
        game->table[i] = malloc (game->numColumns * sizeof (int)) ;
        for (j = 0; j < game->numColumns; j++) {
            scanf("%d", &(game->table[i][j]));
        }
    }
    for (i = 0; i < game->numLines; i++) {
        for (j = 0; j < game->numColumns; j++) {
            printf("%d ", game->table[i][j]);
        }
        printf("\n");
    }
}

int* solveGame (TGame *game) {
    
}

int main () {
    TGame game;
    readGame(&game);
    int* results = solveGame(&game);
}