#include <iostream>
#include <cstddef> 
#include <vector>
using namespace std;

typedef struct NodeT {
    int index;
    int color;
    bool absorbed;
    vector<int> adjList;
} NodeT;

typedef struct GameItemT {
    int color;
    NodeT *node;
} GameItemT;

typedef struct GameT {
    int numColors;
    int numLines;
    int numColumns;
    GameItemT **table;
} GameT;

typedef struct GraphT {
    NodeT *nodes;
    int numNodes;
} GraphT;

void createNode (int i, int j, GameT *game, NodeT *previousNode, GraphT *graph);

GameT readGame () {
    GameT game;
    cin >> game.numLines;
    cin >> game.numColumns;
    cin >> game.numColors;

    int i = 0;
    int j = 0;

    game.table = new GameItemT*[game.numLines];
    for (int i = 0; i < game.numLines; i++) {
        game.table[i] = new GameItemT[game.numColumns];
        for (int j = 0; j < game.numColumns; j++) {
            cin >> game.table[i][j].color;
            game.table[i][j].node = nullptr;
        }
    }
    return game;
}

bool invalidIndex (int i, int j, GameT *game) {
    if (i < 0 || i > game->numLines - 1 || j < 0 || j > game->numColumns - 1) {
        return true;
    }
    return false;
}

bool isDuplicate (NodeT *node1, NodeT *node2) {
    for (auto x : node1->adjList) {
        if (x == node2->index) {
            return true;
        }
    }
    return false;
}

void addEdge (NodeT *node1, NodeT *node2) {
    if (isDuplicate(node1, node2)) {
        return;
    }

    node1->adjList.push_back(node2->index);
    node2->adjList.push_back(node1->index);
}

void createEdges (int i, int j, GameT *game) {
    if (invalidIndex(i, j, game)) {
        return;
    }
    
    GameItemT *currentItem = &(game->table[i][j]);

    if (!invalidIndex(i, j + 1, game)) {
        GameItemT *rightNeighbour = &(game->table[i][j + 1]);
        if (currentItem->color != rightNeighbour->color) {
            addEdge(currentItem->node, rightNeighbour->node);
        }
    }

    if (!invalidIndex(i + 1, j, game)) {
        GameItemT *bottomNeighbour = &(game->table[i + 1][j]);
        if (currentItem->color != bottomNeighbour->color) {
            addEdge(currentItem->node, bottomNeighbour->node);
        }
    }

    if (!invalidIndex(i, j - 1, game)) {
        GameItemT *leftNeighbour = &(game->table[i][j - 1]);
        if (currentItem->color != leftNeighbour->color) {
            addEdge(currentItem->node, leftNeighbour->node);
        }
    }

    if (!invalidIndex(i - 1, j, game)) {
        GameItemT *upperNeighbour = &(game->table[i - 1][j]);
        if (currentItem->color != upperNeighbour->color) {
            addEdge(currentItem->node, upperNeighbour->node);
        }
    }
}

GraphT processGraph (GameT game) {
    GraphT graph;
    graph.numNodes = 0;
    graph.nodes = new NodeT[game.numLines * game.numColumns];

    int i = 0;
    int j = 0;
    for (i = 0; i < game.numLines; i++) {
        for (j = 0; j < game.numColumns; j++) {
            createNode(i, j, &game, nullptr, &graph);
        }
    }
    
    for (i = 0; i < game.numLines; i++) {
        for (j = 0; j < game.numColumns; j++) {
            createEdges(i, j, &game);
        }
    }

    return graph;
}

void createNode (int i, int j, GameT *game, NodeT *previousNode, GraphT *graph) {
    if (invalidIndex(i, j, game)) {
        return;
    }
    
    GameItemT *currentItem = &(game->table[i][j]);

    if (currentItem->node != nullptr) {
        return;
    }

    if (previousNode != nullptr && currentItem->color != previousNode->color) {
        return;
    }

    if (previousNode == nullptr) {
        int index = graph->numNodes;
        graph->numNodes++;
        
        currentItem->node = &(graph->nodes[index]);
        currentItem->node->index = index;
        currentItem->node->color = currentItem->color;
        currentItem->node->absorbed = false;
    } else {
        currentItem->node = previousNode;
    }

    createNode(i, j+1, game, currentItem->node, graph);
    createNode(i+1, j, game, currentItem->node, graph);
    createNode(i, j-1, game, currentItem->node, graph);
    createNode(i-1, j, game, currentItem->node, graph);
}

int* solveGame (GameT *game) {

}

int main () {
    
    GameT game = readGame();
    GraphT graph = processGraph(game);
    int* results = solveGame(&game);

    cout << "\n\n";
    fflush(stdout);
    for (int i = 0; i < graph.numNodes; i++) {
        cout << graph.nodes[i].color;
        cout << "\n";
        cout << "Adj list: ";
        for (auto x : graph.nodes[i].adjList) {
            cout << graph.nodes[x].color;
            cout << " ";
        }
        cout << "\n";
    }

    return 0;
}