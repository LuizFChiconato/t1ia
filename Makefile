all: floodit

floodit: floodit.cpp
	g++ floodit.cpp -o floodit -g

clean:
	rm -rf floodit *.o
